import Home from "./conponent/Home.jsx";
import About from "./conponent/About.jsx";
import Blogs from "./conponent/Blog.jsx";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Nevbar from "./conponent/Navbar.jsx";
import NotFfound from "./conponent/NotFound.jsx";
import Details from "./conponent/Detail.jsx";

function App() {
  return (
    <BrowserRouter>
      <Nevbar />
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="/about" element={<About />}></Route>
        <Route path="/blogs" element={<Blogs />}></Route>
        <Route path="*" element={<NotFfound />}></Route>
        <Route path="/Home" element={<Navigate to="/" />}></Route>
        <Route path="/info" element={<Navigate to="about" />}></Route>
        <Route path="/blog/:id" element={<Details />}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
