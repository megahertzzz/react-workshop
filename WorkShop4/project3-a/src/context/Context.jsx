import {
  createContext,
  useContext,
  useReducer,
  useState,
  useEffect,
} from "react";
import ItemData from "../data/ItemData";
import Reducer from "../reducer/Reducer";

const cartContext = createContext();
const iniState = {
  product: ItemData,
  total: 0,
  amount: 0,
};

export const CartProvider = ({ children }) => {
  const [state, dispatch] = useReducer(Reducer, iniState);
  function formatMoney(money) {
    return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
  }

  const removeItem = (todo) => {
    console.log("ลบรหัสสินค้า = ", todo);
    dispatch({ type: "REMOVE_ITEM", id: todo });
  };

  const increaseItem = (id) => {
    console.log("เพิ่มสินค้าที่รหัสสินค้า = ", id);
    dispatch({ type: "INCREASE_ITEM", id: id });
  };
  const decreaseItem = (id) => {
    console.log("ลบสินค้าที่รหัสสินค้า = ", id);
    dispatch({ type: "DECREASE_ITEM", id: id });
  };

  useEffect(() => {
    dispatch({ type: "CALCULATE_TOTAL" });
  }, [state.product]);

  // console.log("props ที่ส่งไปให้ children ", state);
  return (
    <cartContext.Provider
      value={{ ...state, formatMoney, removeItem, increaseItem, decreaseItem }}
    >
      {children}
    </cartContext.Provider>
  );
};

export const useCartContext = () => {
  return useContext(cartContext);
};
