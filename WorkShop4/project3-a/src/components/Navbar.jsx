import React from "react";
import "../components/Navbar.css";
import { useCartContext } from "../context/Context";
const Navbar = () => {
  const { amount } = useCartContext();
  return (
    <nav className="nav-container">
      <div className="title">Shopping Application</div>
      <div className="quantity-cart">สินค้าในตะกร้า: {amount}</div>
    </nav>
  );
};

export default Navbar;
