import home from "../assets/image/home.svg"

export default function Home(){
    return(
        <div className="container">
            <h2 className="title">หน้าหลัก</h2>
            <img src={home} alt="home" />
        </div>
    )
}