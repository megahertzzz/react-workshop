import { useState , useEffect  } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Header from './compnents/Header'
import AddForm from './compnents/addform'
import Item from './compnents/Item'


function App() {
  const [tasks,setTasks] = useState(JSON.parse(localStorage.getItem("tasks"))||[])
  const [title,setTitle] = useState("")
  const [editId,setEditId] = useState(null)
  const [theme,setTheme]= useState("light");

  //การเรียกใช้ useEffect รูปแบบที่3
  useEffect(()=>{
    localStorage.setItem("tasks",JSON.stringify(tasks))
  },[tasks])


  function deleteTask(id){
    const result = tasks.filter(item=>item.id !== id)
    setTasks(result);
  }
  function editTask(id){
    setEditId(id)
    const editTasks = tasks.find((item)=>item.id === id)
    setTitle(editTasks.title)
  }
  function saveTask(e){
    e.preventDefault();
    if(!title){
      alert("กรุณาป้อนข้อมูล")
    }else if(editId){
      //อัพเดตข้อมูล
      const updateTask= tasks.map((item)=>{
        //รายการใดมีรหัสตรงกับรหัสแก้ไข
        if(item.id === editId){
          return{...item,title:title}
        }
        return item;
      })
      setTasks(updateTask)
      setEditId(null)
      setTitle("")
    }else{
      //เพิ่มรายการใหม่
      const newTask ={
        id:Math.floor(Math.random()*1000),
        title:title
      }
      setTasks([...tasks,newTask])
      setTitle("")
    }
  }
  return (
   
    <div className={"App "+theme}>
      <Header theme= {theme} setTheme={setTheme}/>
      <div className='container'>
        <AddForm title={title} setTitle ={setTitle} saveTask ={saveTask} editId ={editId}/>
        <section>
          {
            tasks.map((data) =>(
              <Item key={data.id} data={data} deleteTask = {deleteTask} editTask={editTask}/>
            ))
          }
        </section>
      </div>
    </div>

      
  );
}

export default App
