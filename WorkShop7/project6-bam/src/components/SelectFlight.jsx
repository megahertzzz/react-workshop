import * as React from "react";
import { styled } from "@mui/material/styles";
import Grid from "@mui/material/Unstable_Grid2";
import Paper from "@mui/material/Paper";
import { Box, IconButton, Icon, Button, Link } from "@mui/material";
import CssBaseline from "@mui/material/CssBaseline";
import "./BookFlight.css";
import "./SelectFlight.css";
import TuneIcon from "@mui/icons-material/Tune";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import PlaceIcon from "@mui/icons-material/Place";
import PlaceOutlinedIcon from "@mui/icons-material/PlaceOutlined";
import FlightTakeoffOutlinedIcon from "@mui/icons-material/FlightTakeoffOutlined";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

export default function SelectFlight() {
  return (
    <>
      <React.Fragment>
        <CssBaseline />
        <Box sx={{ width: "100%" }}>
          <Grid
            container
            rowSpacing={1}
            columnSpacing={{ xs: 0, sm: 2, md: 3 }}
          >
            <Grid xs={12}>
            
              <Item sx={{ p: 0, boxShadow: 0 }}>
                <div className="header" style={{ justifyContent: "center",paddingTop:"1em" }}>
                  <div style={{display:"flex",justifyContent:"space-between"}}>
                  <Link href="/" >
                    <Button className="back-btn" variant="contained" sx={{ color: "white" }}>
                      <ArrowBackIcon />
                    </Button>
                  </Link>
                  </div>
                <div style={{}}>
                <h2
                    style={{
                      color: "white",
                      display: "inline",
                    
                    }}
                  >
                    Select Flight
                  </h2>
                </div>
                 
              
                  <Box
                    display="grid"
                    gridTemplateColumns="repeat(12, 1fr)"
                    gap={1}
                    className="aria-box"
                    sx={{ mt: 2 }}
                  >
                    <Box gridColumn="span 12" sx={{ color: "white" }}>
                      <Icon sx={{ color: "white", display: "inline", ml: 0 }}>
                        <FlightTakeoffOutlinedIcon sx={{ fontSize: "40px" }} />
                      </Icon>
                    </Box>
                    <Box gridColumn="span 12" sx={{ color: "white" }}>
                      <Icon sx={{ color: "white", display: "inline", ml: 0 }}>
                        <PlaceOutlinedIcon />
                      </Icon>
                      -------------------------------------------------
                      <Icon sx={{ color: "white", display: "inline", ml: 0 }}>
                        <PlaceOutlinedIcon />
                      </Icon>
                    </Box>
                    <Box gridColumn="span 4" sx={{ color: "white" }}>
                      CDG
                    </Box>
                    <Box gridColumn="span 4" sx={{ color: "white" }}></Box>
                    <Box gridColumn="span 4" sx={{ color: "white" }}>
                      FLR
                    </Box>
                    <Box gridColumn="span 4" sx={{ color: "white" }}>
                      Paris
                    </Box>
                    <Box gridColumn="span 4" sx={{ color: "white" }}></Box>
                    <Box gridColumn="span 4" sx={{ color: "white" }}>
                      Flance
                    </Box>
                  </Box>
                </div>
              </Item>
            </Grid>
            <Grid xs={12}>
              <Item sx={{ p: 3, pb: 0, boxShadow: 0 }}>
                <Button
                  className="btn-filter"
                  variant="contained"
                  endIcon={<TuneIcon />}
                  sx={{
                    background: "#6b9dae",
                    color: "white",
                    position: "absolute",
                    // top: "31%",
                    // left: "15%",
                    top: "220px",
                    left: "75px",
                    padding: "20px",
                    borderRadius: "3em",
                    margin: "auto",
                  }}
                >
                  Filter
                </Button>
                <Button
                  className="btn-sort"
                  variant="contained"
                  endIcon={<ExpandMoreIcon />}
                  sx={{
                    background: "#6b9dae",
                    color: "white",
                    ml: 1,
                    position: "absolute",
                    // top: "31%",
                    // left: "40%",
                    top: "220px",
                    left: "210px",
                    padding: "20px",
                    borderRadius: "3em",
                    margin: "auto",
                  }}
                >
                  <p>sort by:</p> Quickest
                </Button>
                <p style={{ margin: "0px", padding: "0px" }}>
                  10 Flights Avaliable
                </p>
              </Item>
            </Grid>
            <Grid xs={12} sm={6}>
              <Item
                sx={{
                  p: 2,
                  bgcolor: "#e8f2f4ee",
                  boxShadow: 0,
                  m: 2,
                  borderRadius: "2em",
                }}
              >
                <Box
                  display="grid"
                  gridTemplateColumns="repeat(12, 1fr)"
                  gap={1}
                  className="aria-box"
                >
                  <Box gridColumn="span 4">CDG</Box>
                  <Box gridColumn="span 4"></Box>
                  <Box gridColumn="span 4">FLR</Box>
                  <Box gridColumn="span 4">Lorem ipsum</Box>
                  <Box gridColumn="span 4"></Box>
                  <Box gridColumn="span 4">Lorem ipsum</Box>
                  <Box gridColumn="span 4"> Lorem ipsum</Box>
                  <Box gridColumn="span 4"></Box>
                  <Box gridColumn="span 4">Lorem ipsum </Box>
                  <Box gridColumn="span 12">
                    <Icon sx={{ color: "#02315E", display: "inline", ml: 0 }}>
                      <PlaceIcon />
                    </Icon>
                    --------------------------------------
                    <Icon sx={{ color: "#02315E", display: "inline", ml: 0 }}>
                      <PlaceIcon />
                    </Icon>
                  </Box>
                  <Box gridColumn="span 4">Deport</Box>
                  <Box gridColumn="span 4">1h 45m</Box>
                  <Box gridColumn="span 4">Active</Box>
                  <Box gridColumn="span 4">Sun 24 Jan</Box>
                  <Box gridColumn="span 4">lorem</Box>
                  <Box gridColumn="span 4">Sun 24 Jan</Box>
                  <Box gridColumn="span 4">09:30AM</Box>
                  <Box gridColumn="span 4"></Box>
                  <Box gridColumn="span 4">11:45AM</Box>
                  <Box gridColumn="span 4">
                    <b style={{ fontSize: "16px" }}>AIRFRANCE</b>
                  </Box>
                  <Box gridColumn="span 4"></Box>
                  <Box gridColumn="span 4">
                    <b style={{ fontSize: "16px" }}>$1,181</b> lorem
                  </Box>
                </Box>
              </Item>
            </Grid>
            <Grid xs={12} sm={6}>
              <Item
                sx={{
                  p: 2,
                  bgcolor: "#e8f2f4ee",
                  boxShadow: 0,
                  m: 2,
                  borderRadius: "2em",
                }}
              >
                <Box
                  display="grid"
                  gridTemplateColumns="repeat(12, 1fr)"
                  gap={1}
                  className="aria-box"
                >
                  <Box gridColumn="span 4">CDG</Box>
                  <Box gridColumn="span 4"></Box>
                  <Box gridColumn="span 4">FLR</Box>
                  <Box gridColumn="span 4">Lorem ipsum</Box>
                  <Box gridColumn="span 4"></Box>
                  <Box gridColumn="span 4">Lorem ipsum</Box>
                  <Box gridColumn="span 4"> Lorem ipsum</Box>
                  <Box gridColumn="span 4"></Box>
                  <Box gridColumn="span 4">Lorem ipsum </Box>
                  <Box gridColumn="span 12">
                    <Icon sx={{ color: "#02315E", display: "inline", ml: 0 }}>
                      <PlaceIcon />
                    </Icon>
                    --------------------------------------
                    <Icon sx={{ color: "#02315E", display: "inline", ml: 0 }}>
                      <PlaceIcon />
                    </Icon>
                  </Box>
                  <Box gridColumn="span 4">Deport</Box>
                  <Box gridColumn="span 4">1h 45m</Box>
                  <Box gridColumn="span 4">Active</Box>
                  <Box gridColumn="span 4">Sun 24 Jan</Box>
                  <Box gridColumn="span 4">lorem</Box>
                  <Box gridColumn="span 4">Sun 24 Jan</Box>
                  <Box gridColumn="span 4">09:30AM</Box>
                  <Box gridColumn="span 4"></Box>
                  <Box gridColumn="span 4">11:45AM</Box>
                  <Box gridColumn="span 4">
                    <b style={{ fontSize: "16px" }}>AIRFRANCE</b>
                  </Box>
                  <Box gridColumn="span 4"></Box>
                  <Box gridColumn="span 4">
                    <b style={{ fontSize: "16px" }}>$696</b> lorem
                  </Box>
                </Box>
              </Item>
            </Grid>
          </Grid>
        </Box>
      </React.Fragment>
    </>
  );
}
