import "./App.css";
import Header from "./components/Header";
import Navbar from "./components/Navbar";
import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import {
  Button,
  Grid,
  TextField,
  ThemeProvider,
  Typography,
  createTheme,
} from "@mui/material";
import SearchOutlinedIcon from "@mui/icons-material/SearchOutlined";
import TuneOutlinedIcon from "@mui/icons-material/TuneOutlined";
import Banner from "./components/Banner";
import HotMusic from "./components/HotMusic";
import Recent from "./components/Recent";
import { green, purple } from "@mui/material/colors";
function App() {
  const theme = createTheme({
    palette: {
      primary: {
        main: purple[900]
      },
      secondary: {
        main: green[500],
      },
    },
  });
  return (
    <>
      <ThemeProvider theme={theme}>
        <React.Fragment>
          <CssBaseline />
          <Container maxWidth="sx" className="container">
            <Box sx={{ height: "100vh" }}>
              <Header />
              <Box sx={{ marginTop: "20px" }}>
                <Grid
                  Grid
                  container
                  spacing={2}
                  sx={{ alignItems: "center", p: 1 }}
                >
                  <Grid item xs={9} sm={11}>
                    <TextField
                      id="outlined-search"
                      label={<SearchOutlinedIcon />}
                      variant="outlined"
                      size="small"
                      fullWidth
                    />
                  </Grid>
                  <Grid item xs={3} sm={1}>
                    <Button
                      variant="contained"
                      sx={{
                        backgroundColor: "#fafafa",
                        color: "black",
                      }}
                    >
                      <TuneOutlinedIcon />
                    </Button>
                  </Grid>
                </Grid>
              </Box>

              <Banner />
              <HotMusic />
              <Recent />

              <Box className="navbar">
                <Navbar />
              </Box>
            </Box>
          </Container>
        </React.Fragment>
      </ThemeProvider>
    </>
  );
}

export default App;
