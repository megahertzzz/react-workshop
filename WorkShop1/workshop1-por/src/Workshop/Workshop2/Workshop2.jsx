import { useState } from "react";
import './Workshop2.css';


function Workshop2() {
    const [num1, setNum1] = useState(0);
    const [num2, setNum2] = useState(0);
    const [sum, setSum] = useState(0);
    const [operater, setOperator] = useState("");
    function Calculate (num1,num2, operator) {
        console.log(num1, num2, operater);
        num1 = parseInt(num1);
        num2 = parseInt(num2);
        if (operater === "+") {
            setSum(num1 + num2);
        }
        if (operater === "-") {
            setSum(num1 - num2);
        }
    if (operater === "*") {
      setSum(num1 * num2);
    }
    if (operater === "/") {
      setSum(num1 / num2);
    }
  }
    
    
    return (
        <>
        <h1 style={{textalign:"center"}}>WorkShp2 React</h1>
        <h2>(DAY 4)</h2>
        <div className="contener">
            <div className="Text">
                <p>พัฒนาฟังก์ชันคำนวณเลขสองจำนวนโดยสามารถเลือก บวก ลบ คูณ หาร ได้
                   และเมื่อกดปุ่ม Calculate แสดงผลลัพธ์ที่ถูกต้อง ดังตัวอย่าง
                </p>
            </div>
            <div className="Calculater">
                <div>
                    <input style={{width: "60px", padding: "5px"}}
                        type="number"
                        value={num1}
                        onChange={(e) => setNum1(e.target.value)}
                    />
                    <select style={{width: "60px", padding: "5px"}} value={operater} onChange={(e) => setOperator(e.target.value)}>
                        <option>Operater</option>
                        <option value="+">+</option>
                        <option value="-">-</option>
                        <option value="*">*</option>
                        <option value="/">/</option>
                    </select>
                    <input style={{width: "60px", padding: "5px"}}
                        type="number"
                        value={num2}
                        onChange={(e) => setNum2(e.target.value)}
                    />
                    <span> = {sum}</span>
                </div>
                
            <div className="botton">
                <button  onClick={() => Calculate(num1, num2, operater)}>ผลลัพธ์</button>
            </div>
      </div>
        
        
    </div>   
        </>
    );
}

export default Workshop2;