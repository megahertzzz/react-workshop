import * as React from "react";
// Add these imports
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { AppBar } from "@mui/material";

// Your existing imports here

const harryPotterTheme = createTheme({
  palette: {
    primary: {
      main: "#442C2E", // Gryffindor dark red
    },
    secondary: {
      main: "#FFC700", // Gryffindor gold
    },
    background: {
      default: "#F3EFEF", // Light parchment
      paper: "#E5DBCF", // Parchment
    },
  },
  typography: {
    fontFamily: "Cinzel, Georgia, serif",
    h6: {
      fontWeight: 700,
      letterSpacing: ".3rem",
    },
    h5: {
      fontWeight: 700,
      letterSpacing: ".3rem",
    },
  },
  components: {
    MuiIconButton: {
      styleOverrides: {
        root: {
          color: "#FFC700",
        },
      },
    },
    MuiAvatar: {
      styleOverrides: {
        root: {
          borderColor: "#FFC700",
          borderWidth: 2,
          borderStyle: "solid",
        },
      },
    },
    MuiLink: {
      styleOverrides: {
        root: {
          "&:hover": {
            textDecoration: "underline",
            textDecorationColor: "#FFC700",
          },
        },
      },
    },
  },
});

function ResponsiveAppBar() {
  // Your existing component code

  // Wrap your AppBar component with the ThemeProvider
  return (
    <ThemeProvider theme={harryPotterTheme}>
      <AppBar position="static">
        {/* Your AppBar code */}
      </AppBar>
    </ThemeProvider>
  );
}

export default ResponsiveAppBar;