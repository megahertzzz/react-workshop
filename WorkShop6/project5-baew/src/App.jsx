import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import Navbar from './Navbar'
import User from './User'
import UserCreate from './UserCreate'
import { Routes,Route} from 'react-router-dom'
import UserUpdate from './UserUpdate'
import MediaCard from './Card'
import Table from './Table'

function App() {

  return (
    <div>
      <Navbar/>
      <Routes>
        <Route path="/" element={<User/>} />
        <Route path="create" element={<UserCreate/>} />
        <Route path="update/:id" element={<UserUpdate/>} />
        <Route path='/card' element={<MediaCard/>}></Route>
        <Route path='/table' element={<Table/>}></Route>
      </Routes>
    </div>
  )
}

export default App
