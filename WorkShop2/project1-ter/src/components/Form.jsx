import "./Form.css";
export default function Form(props) {
  const { title, setTitle, saveTask, updateid} = props;
  return (
    <>
      <h2>เพิ่ม</h2>
      <form onSubmit={saveTask}>
        <div className="form-control">
          <input
            type="text"
            className="addname"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
          <button type="submit" className="submit-btn">
            {updateid ? "อัพเดต" : "เพิ่ม"}
          </button>
        </div>
      </form>
    </>
  );
}
