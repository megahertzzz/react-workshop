import { useState } from "react";
import "./addForm.css"

export default function AddForm(props){
    const {students,setStudent} = props;
    const[name,setName] = useState("");
    const [gender,setgender] = useState("male");
    function saveStudents(e){
        e.preventDefault();
        
        if(!name){
            alert("กรุณาป้อนข้อมูล")
        }else{
        const newStudent={
            id:Math.floor(Math.random()*1000),
            name:name,
            gender:gender
        }
        setStudent([...students,newStudent])
        setName("")
        setgender("male")
    }
    }
    return(
        <section className="container">
            <form onSubmit={saveStudents} >
                <label>ชื่อนักเรียน</label>
                <input type="text" name="name" value={name} onChange={(e)=> setName(e.target.value)} />
                <select value={gender} onChange={(e)=>setgender(e.target.value)}>
                    <option value="male">ชาย</option>
                    <option value="female">หญิง</option>

                </select>

                <button type="submit" className="btn-add">บันทึก</button>
            </form>
        </section>
    );

}