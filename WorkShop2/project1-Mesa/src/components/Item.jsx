import "./Item.css";
import { BsTrashFill } from "react-icons/bs";
import { AiFillEdit } from "react-icons/ai";

export default function Item(props){
    const {data,deleteTask,editTask} = props

    return(
        <div className="list-item">
            <p className="title">{data.title}</p>
            <div className="button-container">
                <BsTrashFill className="btn" onClick={()=>deleteTask(data.id)}>ลบ</BsTrashFill>
                <AiFillEdit className="btn" onClick={()=>editTask(data.id)}>แก้ไข</AiFillEdit>
                
            </div>
        </div>
    );
}