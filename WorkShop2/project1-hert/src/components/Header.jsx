import "./Header.css";
import { FaMoon, FaSun } from "react-icons/fa";

function Header(props) {
  const { theme, setTheme } = props;

  function ToggleTheme() {
    if (theme === "light") {
      setTheme("dark");
    } else {
      setTheme("light");
    }
  }

  return (
    <header>
      <div className="logo">
        <span>Task Magnagement</span>
      </div>
      <div className="theme-container">
        <span>{theme === "light" ? "โหมดกลางวัน" : "โหมดกลางคืน"}</span>
        <span className="icon" onClick={ToggleTheme}>
          {theme === "light" ? <FaSun /> : <FaMoon />}
        </span>
      </div>
    </header>
  );
}

export default Header;
