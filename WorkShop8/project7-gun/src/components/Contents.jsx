import { Box, Typography } from "@mui/material";
import CreditCardIcon from "@mui/icons-material/CreditCard";
export default function Content() {
  return (
    <>
    <Box
      sx={{ marginTop: "20px", p: 2, bgcolor: "#eeeeee", borderRadius: "25px" }}
    >
      <Box sx={{ display: "flex", justifyContent: "space-between" }}>
        <Typography>Checking</Typography>
        <Typography sx={{fontWeight:"900"}}>$4,567</Typography>
      </Box>
      <Box sx={{ display: "flex", paddingTop: "10px" }}>
        <Typography
          sx={{
            bgcolor: "#212121",
            p: 0.5,
            borderRadius: "50%",
            width: "35px",
            height: "35px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            color: "#ffffff",
          }}
        >
          <CreditCardIcon />
        </Typography>
        <Typography> &nbsp; ***** 5678 Bank of America</Typography>
      </Box>
    </Box>
    <Box
      sx={{ marginTop: "20px", p: 2, bgcolor: "#eeeeee", borderRadius: "25px" }}
    >
      <Box sx={{ display: "flex", justifyContent: "space-between" }}>
        <Typography>Checking</Typography>
        <Typography sx={{fontWeight:"900"}}>$4,567</Typography>
      </Box>
      <Box sx={{ display: "flex", paddingTop: "10px" }}>
        <Typography
          sx={{
            bgcolor: "#212121",
            p: 0.5,
            borderRadius: "50%",
            width: "35px",
            height: "35px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            color: "#ffffff",
          }}
        >
          <CreditCardIcon />
        </Typography>
        <Typography> &nbsp; ***** 5678 Bank of America</Typography>
      </Box>
    </Box>
    <Box
      sx={{ marginTop: "20px", p: 2, bgcolor: "#eeeeee", borderRadius: "25px" }}
    >
      <Box sx={{ display: "flex", justifyContent: "space-between" }}>
        <Typography>Checking</Typography>
        <Typography sx={{fontWeight:"900"}}>$4,567</Typography>
      </Box>
      <Box sx={{ display: "flex", paddingTop: "10px" }}>
        <Typography
          sx={{
            bgcolor: "#212121",
            p: 0.5,
            borderRadius: "50%",
            width: "35px",
            height: "35px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            color: "#ffffff",
          }}
        >
          <CreditCardIcon />
        </Typography>
        <Typography> &nbsp; ***** 5678 Bank of America</Typography>
      </Box>
    </Box>
    <Box
      sx={{ marginTop: "20px", p: 2, bgcolor: "#eeeeee", borderRadius: "25px" }}
    >
      <Box sx={{ display: "flex", justifyContent: "space-between" }}>
        <Typography>Checking</Typography>
        <Typography sx={{fontWeight:"900"}}>$4,567</Typography>
      </Box>
      <Box sx={{ display: "flex", paddingTop: "10px" }}>
        <Typography
          sx={{
            bgcolor: "#212121",
            p: 0.5,
            borderRadius: "50%",
            width: "35px",
            height: "35px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            color: "#ffffff",
          }}
        >
          <CreditCardIcon />
        </Typography>
        <Typography> &nbsp; ***** 5678 Bank of America</Typography>
      </Box>
    </Box>
    </>
  );
}
