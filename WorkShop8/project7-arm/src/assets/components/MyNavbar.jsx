import { AppBar, Box, Button, Toolbar } from "@mui/material";
import HomeIcon from "@mui/icons-material/Home";
import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
import DataUsageIcon from "@mui/icons-material/DataUsage";
import SettingsIcon from "@mui/icons-material/Settings";
import "./MyNavbar.css";

export default function MyNavbar() {
  return (
    <Box>
      <AppBar>
        <Toolbar>
          <Button>
            <HomeIcon />
          </Button>
          <Button>
            <CalendarMonthIcon />
          </Button>
          <Button>
            <DataUsageIcon />
          </Button>
          <Button>
            <SettingsIcon />
          </Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
