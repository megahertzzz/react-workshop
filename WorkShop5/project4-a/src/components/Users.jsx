import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Avatar from "@mui/material/Avatar";
// import Link from "@mui/material/Link";
import ButtonGroup from "@mui/material/ButtonGroup";
import { Link } from "react-router-dom";

export default function Users() {
  const [user, setUser] = React.useState([]);
  React.useEffect(() => {
    fetch("https://www.melivecode.com/api/users")
      .then((response) => response.json())
      .then((data) => setUser(data));
  }, [user]);

  const deleteUser = (userID) => {
    console.log("User ID ที่มีการ Update ", userID);

    const myHeader = { "Content-Type": "application/json" };
    const raw = JSON.stringify({
      id: userID,
    });

    const requestOption = {
      method: "DELETE",
      headers: myHeader,
      body: raw,
      redirect: "follow",
    };

    fetch("https://www.melivecode.com/api/users/delete", requestOption)
      .then((response) => response.json())
      .then((result) => {
        console.log(result);
        return result;
      })
      .then((result) => {
        if (result["status"] === "ok") {
          console.log(result.message);
        }
      })
      .catch((error) => console.log("error", error));
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="lg" sx={{ p: 2 }}>
        <Paper sx={{ p: 2 }}>
          <Box sx={{ display: "flex" }}>
            <Box sx={{ flexGrow: 1 }}>
              <Typography variant="h6" gutterBottom>
                Users
              </Typography>
            </Box>
            <Box>
              <Link to="/userCreate">
                <Button variant="contained">Create</Button>
              </Link>
            </Box>
          </Box>
        </Paper>
        <TableContainer component={Paper} sx={{ marginTop: 2 }}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Users</TableCell>
                <TableCell align="center">Avatar</TableCell>
                <TableCell align="right">ID</TableCell>
                <TableCell align="right">First Name</TableCell>
                <TableCell align="right">Last Name</TableCell>
                <TableCell align="right">Action</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {user.map((row) => (
                <TableRow
                  key={row.id}
                  sx={{
                    "&:last-child td, &:last-child th": {
                      border: 0,
                    },
                    p: 2,
                  }}
                >
                  <TableCell component="th" scope="row">
                    {row.username}
                  </TableCell>
                  <TableCell align="center">
                    <Box display="flex" justifyContent="center">
                      <Avatar alt={row.fname} src={row.avatar} />
                    </Box>
                  </TableCell>
                  <TableCell align="right">{row.id}</TableCell>
                  <TableCell align="right">{row.fname}</TableCell>
                  <TableCell align="right">{row.lname}</TableCell>
                  <TableCell align="right">
                    <ButtonGroup
                      color="secondary"
                      aria-label="medium secondary button group"
                    >
                      <Link to={`/updateUser/${row.id}`}>
                        <Button variant="contained" sx={{ mx: 2 }}>
                          Edit
                        </Button>
                      </Link>
                      <Button
                        onClick={() => deleteUser(row.id)}
                        variant="contained"
                      >
                        Delete
                      </Button>
                    </ButtonGroup>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Container>
    </React.Fragment>
  );
}
