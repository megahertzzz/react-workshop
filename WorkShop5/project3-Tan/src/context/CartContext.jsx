import { createContext, useContext, useReducer ,useEffect} from "react";
import Products from "../data/product.jsx";
import CartReducer from "../reducer/cartReducer.jsx";
//การสร้าง State 

const CartContext = createContext();  
const initState = {
  products: Products,
  total: 0,
  amount: 0,
};
export const CartProvider=({ children }) => {
  const [state, dispatch] = useReducer(CartReducer, initState);
  function formatMoney(money){ //5000 -> : 5,000
    return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
  }

  function removeItem(id,name){
    console.log("ลบสินค้ารหัส = "+id+"("+name+")")
    dispatch({type:"REMOVE",payload:id})
  }

  function addQuantity(id){
    console.log("เพิ่มปริมาณสินค้า = "+id)
    dispatch({type:"ADD",payload:id})
  }

  function delQuantity(id){
    console.log("ลดปริมาณสินค้า = "+id)
    dispatch({type:"Delete",payload:id})
  }

  useEffect(() => {
    console.log("คำนวนหาผลรวม");
    dispatch({type:"CALCULATE_TOTAL"})
  }, [state.products]);
  return (
    <CartContext.Provider value={{ ...state, formatMoney,removeItem,addQuantity,delQuantity }}>
      {children}
    </CartContext.Provider>
  );
};

export const useCart = () => {
  return useContext(CartContext);
};